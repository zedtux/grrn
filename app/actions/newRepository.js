import log from 'loglevel';

export const SHOW_MODAL = 'SHOW_MODAL';
export const CLOSE_MODAL = 'CLOSE_MODAL';
export const SAVE_AND_CLOSE_MODAL = 'SAVE_AND_CLOSE_MODAL';
export const UPDATE_MODAL_VALUE = 'UPDATE_MODAL_VALUE';
export const MODAL_ERROR = 'MODAL_ERROR';

export function show() {
  log.debug('[actions/newRepository] show() ...');
  return {
    type: SHOW_MODAL
  };
}

export function close() {
  log.debug('[actions/newRepository] close() ...');
  return {
    type: CLOSE_MODAL
  };
}

export function saveAndClose() {
  log.debug('[actions/newRepository] saveAndClose() ...');

  let returnType = SAVE_AND_CLOSE_MODAL,
      errorMessage = '',
      inputClass = '',
      remoteRepositoryUser = '',
      remoteRepositoryName = '';

  return (dispatch, getState) => {
    const { modalValue } = getState();

    let splittedModalValue = modalValue.match(/^([a-z]+)\/([a-z]+)/);

    if (modalValue == '') {
      returnType = MODAL_ERROR
      errorMessage = "Cannot be blank"
      inputClass = 'error';
    } else if (splittedModalValue) {
      remoteRepositoryUser = splittedModalValue[1]
      remoteRepositoryName = splittedModalValue[2]
    } else {
      returnType = MODAL_ERROR
      errorMessage = "Malformed repository URL"
      inputClass = 'error';
    }

    dispatch({
      type: returnType,
      message: errorMessage,
      inputClass: inputClass,
      remoteRepositoryUser: remoteRepositoryUser,
      remoteRepositoryName: remoteRepositoryName
    });
  };
}

export function modalHandleValueChange(string) {
  log.debug('[actions/newRepository] modalHandleValueChange(string:"' + string + '") ...');
  return {
    type: UPDATE_MODAL_VALUE,
    value: string
  };
}
