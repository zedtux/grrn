import React, { Component, PropTypes } from 'react';
import { Modal, Button, Input } from 'react-bootstrap';
import log from 'loglevel';


export default class NewRepository extends Component {
  static propTypes = {
    close: PropTypes.func.isRequired,
    saveAndClose: PropTypes.func.isRequired,
    showModal: PropTypes.bool.isRequired,
    modalValue: PropTypes.string.isRequired,
    modalHandleValueChange: PropTypes.func.isRequired,
    modalErrorMessage: PropTypes.string.isRequired,
    modalInputClass: PropTypes.string.isRequired
  }

  render() {
    log.debug('[components/NewRepository] render() ...');
    const {
      close,
      saveAndClose,
      showModal,
      modalValue,
      modalHandleValueChange,
      modalErrorMessage,
      modalInputClass
    } = this.props

    let inputProps;

    // Set the bsStyle of the Input component when a CSS class is given.
    // This prevent react-bootstrap error when passing an empty string
    if (modalInputClass.length)
      inputProps = {
        bsStyle: modalInputClass
      }

    return (
      <Modal show={showModal} onHide={close}>
        <Modal.Header closeButton>
          <Modal.Title>Add a new repository</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h4>Provide the Github repository URL as <code>username:repositoryname</code></h4>
          <br />
          <Input
            type="text"
            id="repositoryUrl"
            value={modalValue}
            placeholder="Enter text"
            help={modalErrorMessage}
            hasFeedback
            {...inputProps}
            ref="input"
            groupClassName="group-class"
            labelClassName="label-class"
            onChange={(e) => modalHandleValueChange(e.currentTarget.value)} />
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={close}>Cancel</Button>
          <Button onClick={saveAndClose} bsStyle="primary">Add</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
