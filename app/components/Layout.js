import React, { Component, PropTypes } from 'react';
import { Glyphicon } from 'react-bootstrap';
import NewRepositoryModal from '../containers/NewRepositoryModal';
import log from 'loglevel';


export default class Layout extends Component {
  static propTypes = {
    show: PropTypes.func.isRequired
  }

  render() {
    log.debug('[components/Layout] render() ...');
    const { show } = this.props
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-2">
            <div className="col-sm-3 col-md-2 sidebar">
              <ul className="nav nav-sidebar">
                <li>
                  <a href="#" onClick={show}>
                    <Glyphicon glyph="plus" />
                    New repository
                  </a>
                </li>
                <li className="sidebar-section sub-header">Repositories</li>
              </ul>
            </div>
          </div>
          <div className="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            {this.props.children}
          </div>
        </div>
        <NewRepositoryModal />
      </div>
    );
  }
}
