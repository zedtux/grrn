import {
  SHOW_MODAL,
  CLOSE_MODAL,
  UPDATE_MODAL_VALUE,
  SAVE_AND_CLOSE_MODAL,
  MODAL_ERROR
} from '../actions/newRepository';

export function showModal(state = false, action) {
  switch (action.type) {
  case SHOW_MODAL:
    return true
  case CLOSE_MODAL:
  case SAVE_AND_CLOSE_MODAL:
    return false
  default:
    return state
  }
}

export function modalValue(state = '', action) {
  switch (action.type) {
  case MODAL_ERROR:
    return state
  case UPDATE_MODAL_VALUE:
    return action.value
  case SAVE_AND_CLOSE_MODAL:
  default:
    return ''
  }
}

export function modalErrorMessage(state = '', action) {
  switch (action.type) {
  case MODAL_ERROR:
    return action.message
  default:
    return ''
  }
}

export function modalInputClass(state = '', action) {
  switch (action.type) {
  case MODAL_ERROR:
  case SAVE_AND_CLOSE_MODAL:
    return action.inputClass
  default:
    return ''
  }
}
