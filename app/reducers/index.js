import { combineReducers } from 'redux';
import counter from './counter';
import {
  showModal,
  modalValue,
  modalErrorMessage,
  modalInputClass
} from './modal';
import log from 'loglevel';

log.debug('[reducers/index] combineReducers() ...');
const rootReducer = combineReducers({
  counter,
  showModal,
  modalValue,
  modalErrorMessage,
  modalInputClass
});

export default rootReducer;
