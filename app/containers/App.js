import React, { Component, PropTypes } from 'react';
import LayoutPage from '../containers/LayoutPage';
import log from 'loglevel';

export default class App extends Component {
  static propTypes = {
    children: PropTypes.element.isRequired
  };

  render() {
    log.debug('[containers/App] Rendering App ...');
    return (
      <LayoutPage>
        <div>
          {this.props.children}
          {
          (() => {
            if (process.env.NODE_ENV !== 'production') {
              const DevTools = require('./DevTools');
              return <DevTools />;
            }
          })()
        }
        </div>
      </LayoutPage>
    );
  }
}
