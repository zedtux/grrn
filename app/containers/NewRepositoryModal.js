import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import NewRepository from '../components/NewRepository';
import * as ActionCreators from '../actions/newRepository';
import log from 'loglevel';

function mapStateToProps(state) {
  return {
    showModal: state.showModal,
    modalValue: state.modalValue,
    modalErrorMessage: state.modalErrorMessage,
    modalInputClass: state.modalInputClass
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(NewRepository);
