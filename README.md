# GRRN

**GRRN** stands for **G**ithub **R**epositories **R**eleases **N**otifier.

It will watch your favorites repositories and notify you (using your operating system notification system) in order to inform you about a new release.

## Status

This repository is under heavy development.

You can still give it a try if you'd like too but forget about opening issues :)

## Usage

As this repository is a fork of the Electron React boilerplate repository, you have to follow the Webpack instructions:

In a first terminal:

```
$ npm install
$ npm run hot-server
```

In a second terminal:

```
$ npm run start-hot
```

## Contributing

1. Fork it ( https://github.com/zedtux/grrn/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
